## Requirements

- List frontend jobs from RemoteOK
- Link each job to its specific page on RemoteOK
- Deploy to a production environment

## Design

### Architecture

### User Interface

## Implementation

- Typescript
- React.js

## Testing

### Unit tests: Jest

### E2E: Cypress

## Deployment

- Vercel
