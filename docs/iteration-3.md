## Requirements

## Design

### Architecture

### User Interface

## Implementation

- Typescript
- React.js

## Testing

- Unit tests: Jest
- E2E: Cypress

## Deployment

- Vercel