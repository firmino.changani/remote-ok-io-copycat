# RemoteOK.io Copycat

Reimplementing RemoteOK.io using React.js and Typescript

## Iterations

- [Iteration 1 (IT-1): Kick start](./docs/iteration-1.md)
- [Iteration 2 (IT-2): Booletproof](./docs/iteration-2.md)
- [Iteration 3 (IT-3): Shine like a diamond](./docs/iteration-3.md)

## Architecture

![Package diagram](./docs/diagrams/project-architecture-package-diagram.png)
