export interface Job {
  slug:         string;
  id:           string;
  epoch:        string;
  date:         string;
  company:      string;
  company_logo: string;
  position:     string;
  tags:         string[];
  logo:         string;
  description:  string;
  location:     string;
  url:          string;
  apply_url:    string;
}
