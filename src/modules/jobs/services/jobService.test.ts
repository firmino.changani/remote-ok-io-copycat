import { jobService } from "./jobService";

describe("JobService.ts", () => {
  it(".getJobs() should return an array", async () => {
    const jobs = await jobService.getJobs();
    expect(Array.isArray(jobs)).toBe(true);
  });

  it(".getJobs() should contain more than 0 elements", async () => {
    const jobs = await jobService.getJobs();
    expect(jobs.length > 0).toBe(true);
  });

  it(".getJobs() first element should contain required properties", async () => {
    const [job] = await jobService.getJobs();

    expect(job.position).toBeTruthy();
    expect(job.company).toBeTruthy();
    expect(job.url).toBeTruthy();
  });
});
