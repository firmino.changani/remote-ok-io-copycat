import { Job } from "./job";
import { HttpService, httpService } from "../../../common/services/httpService/httpService";

export interface JobService {
  getJobs(): Promise<Job[]>;
}

export class JobService implements JobService {
  private REMOTE_OK_API = "https://remoteok.io/api";

  constructor(public httpService: HttpService) {}

  async getJobs(): Promise<Job[]> {
    try {
      const [, ...jobs] = await this.httpService.get(this.REMOTE_OK_API);
      return jobs as Job[];
    } catch (error) {
      throw new Error("A error has occured while trying to fetch RemoteOK.io's API");
    }
  }
}

export const jobService = new JobService(httpService);
