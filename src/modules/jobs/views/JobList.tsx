import { Job } from "../services/job";
import { useState, useEffect } from "react";
import { jobService } from "../services/jobService";
import { JobListItem } from "../components/jobListItem/JobListItem";

export function JobList() {
  const [error, setError] = useState("");
  const [jobs, setJobs] = useState([] as Job[]);

  useEffect(() => {
    async function init() {
      try {
        const jobList: Job[] = await jobService.getJobs();
        setJobs(jobList);
      } catch {
        setError("An error occured trying to fetch jobs from RemoteOK.io");
      }
    }

    init();
  }, []);

  if (error) {
    throw new Error(error);
  }

  const renderJobs = () => {
    return jobs.map((job, index) => <JobListItem key={`job_${index}`} job={job} />);
  };

  return <section>{renderJobs()}</section>;
}
