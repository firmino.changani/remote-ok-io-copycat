import { Job } from "../../services/job";
import { DateUtil } from "../../../../common/utils/dateUtil/dateUtil";
import { ApplyButtonContainer, JobListItemContainer, JobMetaData } from "./JobListItemStyles";

interface JobListItemProps {
  job: Job;
}

function renderTags(tags: string[]) {
  return tags.map((tag, index) => <span key={index}>{tag}</span>);
}

function renderLocation(location: string): JSX.Element | null {
  return <span>{location}</span> || null;
}

export function JobListItem({ job }: JobListItemProps) {
  return (
    <JobListItemContainer>
      <div>
        <span>{job.company}</span>
        <span> {DateUtil.toNow(new Date(job.date))}</span>
        <h1>
          <a href={job.url}>{job.position} </a>
        </h1>
        {renderLocation(job.location)}
      </div>

      <JobMetaData>{renderTags(job.tags)}</JobMetaData>

      <ApplyButtonContainer className="job-list-item__apply-button">
        <a role="button" href={job.url} title="Apply from RemoteOK.io">
          Apply
        </a>
      </ApplyButtonContainer>
    </JobListItemContainer>
  );
}
