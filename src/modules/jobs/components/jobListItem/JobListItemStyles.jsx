import styled from "styled-components";

export const JobListItemContainer = styled.article`
  display: grid;
  padding: 10px;
  border-radius: 5px;
  margin-bottom: 30px;
  align-items: center;
  border: 1px solid #fff;
  grid-template-columns: 40% 45% 15%;

  div {
    // border: 1px solid red;
  }

  &:hover {
    .job-list-item__apply-button a {
      opacity: 1;
      visibility: visible;
    }
  }

  h1 {
    font-size: 18px;
    font-weight: 900;
    text-transform: capitalize;

    a {
      color: #fff;

      &:hover {
        text-decoration: underline;
      }
    }
  }
`;

export const JobMetaData = styled.div`
  span {
    height: 19px;
    font-size: 12px;
    color: #46466e;
    padding: 2px 5px;
    font-weight: 900;
    border-radius: 3px;
    margin: 0 5px 5px 0;
    display: inline-block;
    background-color: #fff;
    text-transform: uppercase;
  }
`;

export const ApplyButtonContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;

  a {
    opacity: 0;
    width: 100%;
    color: #fff;
    padding: 10px 0;
    font-size: 12px;
    font-weight: 900;
    text-align: center;
    visibility: hidden;
    border-radius: 3px;
    border: 2px solid #fff;
    transition: opacity 0.5s;
    text-transform: uppercase;
  }
`;
