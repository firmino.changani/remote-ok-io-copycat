export interface HttpService {
  get<T>(url: string): Promise<T>;
}

export class HttpService implements HttpService {
  async get<T>(url: string): Promise<T> {
    try {
      const response = await fetch(url);
      const data = await response.json();

      return data as T;
    } catch (error) {
      return Promise.reject(error);
    }
  }
}

export const httpService = new HttpService();
