import { httpService } from './httpService';
describe("HttpService.ts", () => {
  it("Should get data from an API", async () => {
    const data = await httpService.get<Record<string, string>[]>("https://jsonplaceholder.typicode.com/users");
    expect(Array.isArray(data)).toBe(true);
    expect(data.length > 0).toBe(true);
  });

  it("Should reject when the API url isn't valid", () =>{
    const request = () => httpService.get("/some-api-endpoint");
    expect(request).rejects.toThrow();
  });
});