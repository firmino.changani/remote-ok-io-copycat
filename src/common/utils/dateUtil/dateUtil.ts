import dayjs from "dayjs";
import relativeTime from "dayjs/plugin/relativeTime";

dayjs.extend(relativeTime);

export class DateUtil {
  static toNow(date: Date) {
    return dayjs().to(dayjs(date));
  }
}
