import React from "react";
import styled from "styled-components";

interface ErrorBoundaryState {
  error: string;
  hasError: boolean;
}

const ErrorContainer = styled.div`
  margin-bottom: 30px;

  h1 {
    font-weight: 900;
  }
`;

export class ErrorBoundary extends React.Component {
  state!: ErrorBoundaryState;

  constructor(props: { children: React.ReactNode }) {
    super(props);
    this.state = { hasError: false, error: "" };
  }

  static getDerivedStateFromError(error: Error) {
    return { hasError: true, error: error.message };
  }

  renderError() {
    return (
      <ErrorContainer>
        <h1>Something went wrong :(</h1>
        <p>{this.state.error}</p>
      </ErrorContainer>
    );
  }

  renderChildrenComponents() {
    return this.props.children;
  }

  render() {
    return this.state.hasError ? this.renderError() : this.renderChildrenComponents();
  }
}
