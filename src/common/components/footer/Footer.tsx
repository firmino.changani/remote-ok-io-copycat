import styled from "styled-components";

const FooterContainer = styled.footer`
  font-size: 12px;

  a {
    color: #fff;
  }
`;

export function Footer() {
  return (
    <FooterContainer>
      <p>
        All jobs displayed on this site belongs to <a href="remoteok.io">RemoteOK.io</a>.
      </p>
    </FooterContainer>
  );
}
