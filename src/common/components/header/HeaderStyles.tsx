import styled from "styled-components";

export const HeaderContainer = styled.header`
  display: flex;
  align-items: center;
  margin-bottom: 30px;
  justify-content: space-between;

  a {
    color: #fff;

    &:hover {
      text-decoration: underline;
    }
  }
`;

export const HeaderLogo = styled.div`
  font-size: 20px;
  font-weight: 900;
  text-transform: lowercase;
`;

export const HeaderNav = styled.nav`
  font-size: 10px;
  font-weight: 900;

  ul {
    display: flex;
    list-style: none;
    text-transform: uppercase;

    li {
      margin-left: 10px;
    }
  }
`;
