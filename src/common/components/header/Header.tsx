import { HeaderContainer, HeaderLogo, HeaderNav } from "./HeaderStyles";

function getHost(): string {
  return window.location.origin;
}

export function Header() {
  return (
    <HeaderContainer className="header">
      <HeaderLogo>
        <a href={getHost()}>RemoteOK.io Copycat</a>
      </HeaderLogo>
      <HeaderNav>
        <ul>
          <li>
            <a href="https://gitlab.com/firmino.changani/remote-ok-io-copycat">Gitlab</a>
          </li>
          <li>
            <a href="https://changani.me" title="Get in touch with me">
              By Firmino Changani
            </a>
          </li>
        </ul>
      </HeaderNav>
    </HeaderContainer>
  );
}
