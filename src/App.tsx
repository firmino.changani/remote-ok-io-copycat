import "./common/styles/app.css";
import { MainContainer, Grid } from "./AppStyles";
import { JobList } from "./modules/jobs/views/JobList";
import { Footer } from "./common/components/footer/Footer";
import { Header } from "./common/components/header/Header";
import { ErrorBoundary } from "./common/views/errors/ErrorBoundary";

export const App = () => {
  return (
    <MainContainer>
      <Grid>
        <Header></Header>
        <ErrorBoundary>
          <JobList></JobList>
        </ErrorBoundary>
        <Footer></Footer>
      </Grid>
    </MainContainer>
  );
};
