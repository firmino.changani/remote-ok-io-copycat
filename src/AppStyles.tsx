import styled from "styled-components";

export const MainContainer = styled.main`
  padding: 10px;
  color: #fff;
  min-height: 100vh;
  background-color: #46466e;
`;

export const Grid = styled.section`
  width: 70%;
  margin: 0 auto;
  padding: 30px 0;

  @media (max-width: 1024px) {
    width: 90%;
  }

  @media (min-width: 1440px) {
    width: 1080px;
  }
`;
